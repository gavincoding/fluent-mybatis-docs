package cn.org.atool.fluent.mybatis.demo3.dao.impl;

import cn.org.atool.fluent.mybatis.demo3.dao.base.HelloWorldBaseDao;
import cn.org.atool.fluent.mybatis.demo3.dao.intf.HelloWorldDao;
import org.springframework.stereotype.Repository;

/**
 * @ClassName HelloWorldDaoImpl
 * @Description HelloWorldEntity数据操作实现类
 *
 * @author generate code
 */
@Repository
public class HelloWorldDaoImpl extends HelloWorldBaseDao implements HelloWorldDao {
}