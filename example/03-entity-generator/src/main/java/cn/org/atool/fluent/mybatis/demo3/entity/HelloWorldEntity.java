package cn.org.atool.fluent.mybatis.demo3.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.IEntity;
import java.io.Serializable;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * HelloWorldEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@Data
@Accessors(
    chain = true
)
@FluentMybatis(
    table = "hello_world"
)
public class HelloWorldEntity implements IEntity {
  private static final long serialVersionUID = 1L;

  @TableId("id")
  private Long id;

  @TableField("gmt_create")
  private Date gmtCreate;

  @TableField("gmt_modified")
  private Date gmtModified;

  @TableField("is_deleted")
  private Integer isDeleted;

  @TableField("say_hello")
  private String sayHello;

  @TableField("your_name")
  private String yourName;

  @Override
  public Serializable findPk() {
    return this.id;
  }
}
