package cn.org.atool.fluent.mybatis.demo3;

import cn.org.atool.generator.FileGenerator;

public class EntityGeneratorDemo2 {
    private static final String url = "jdbc:mysql://localhost:3306/fluent_mybatis?useUnicode=true&characterEncoding=utf8";

    private static final String baseDir = System.getProperty("user.dir") + "/example/03-entity-generator/";

    public static void main(String[] args) throws Exception {
        FileGenerator.build(true, false)
            .globalConfig(g -> g
                /** 设置src/java 所在的路径 **/
                .setOutputDir(baseDir + "src/main/java/")
                /** 设置package **/
                .setBasePackage("cn.org.atool.fluent.mybatis.demo3")
                /** 设置数据库连接 **/
                .setDataSource(url, "root", "password")
            )
            .tables(t -> t
                    /** 设置需要生成Entity类的表 **/
                    .table("hello_world")
                //.table("其它表")
                //.table("其它表")
            )
            .execute();
    }
}