package cn.org.atool.fluent.mybatis.demo4;

import cn.org.atool.generator.FileGenerator;
import cn.org.atool.generator.annotation.Table;
import cn.org.atool.generator.annotation.Tables;

public class MoreEntityMain {
    private static final String url = "jdbc:mysql://localhost:3306/fluent_mybatis?useUnicode=true&characterEncoding=utf8";

    public static void main(String[] args) throws Exception {
        FileGenerator.build(Abc.class);
    }

    @Tables(
        url = url, username = "root", password = "password",
        basePack = "cn.org.atool.fluent.mybatis.demo4",
        srcDir = "example/04-advanced-generator/src/main/java",
        testDir = "example/04-advanced-generator/src/main/java",
        daoDir = "example/05-more-syntax/src/main/java",
        gmtCreated = "gmt_create", gmtModified = "gmt_modified", logicDeleted = "is_deleted",
        tables = {
            @Table({"hello_world", "user", "receiving_address"})
        }
    )
    static class Abc {
    }
}