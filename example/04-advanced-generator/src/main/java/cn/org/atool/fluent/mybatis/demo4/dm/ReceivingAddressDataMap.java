package cn.org.atool.fluent.mybatis.demo4.dm;

import java.lang.Boolean;
import java.lang.String;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.test4j.module.ICore.DataMap;
import org.test4j.module.database.IDatabase;
import org.test4j.module.database.annotations.ColumnDef;
import org.test4j.module.database.annotations.ScriptTable;
import org.test4j.tools.datagen.IDataMap;
import org.test4j.tools.datagen.KeyValue;

/**
 * ReceivingAddressDataMap: 表(实体)数据对比(插入)构造器
 *
 * @author Powered By Test4J
 */
@ScriptTable("receiving_address")
public class ReceivingAddressDataMap extends DataMap<ReceivingAddressDataMap> {
  private boolean isTable;

  private Supplier<Boolean> supplier = () -> this.isTable;

  @ColumnDef(
      value = "id",
      type = "bigint(21) unsigned",
      primary = true,
      autoIncrease = true
  )
  public final transient KeyValue<ReceivingAddressDataMap> id = new KeyValue(this, "id", "id", supplier);

  @ColumnDef(
      value = "gmt_create",
      type = "datetime"
  )
  public final transient KeyValue<ReceivingAddressDataMap> gmtCreate = new KeyValue(this, "gmt_create", "gmtCreate", supplier);

  @ColumnDef(
      value = "gmt_modified",
      type = "datetime"
  )
  public final transient KeyValue<ReceivingAddressDataMap> gmtModified = new KeyValue(this, "gmt_modified", "gmtModified", supplier);

  @ColumnDef(
      value = "is_deleted",
      type = "tinyint(2)"
  )
  public final transient KeyValue<ReceivingAddressDataMap> isDeleted = new KeyValue(this, "is_deleted", "isDeleted", supplier);

  @ColumnDef(
      value = "city",
      type = "varchar(50)"
  )
  public final transient KeyValue<ReceivingAddressDataMap> city = new KeyValue(this, "city", "city", supplier);

  @ColumnDef(
      value = "detail_address",
      type = "varchar(100)"
  )
  public final transient KeyValue<ReceivingAddressDataMap> detailAddress = new KeyValue(this, "detail_address", "detailAddress", supplier);

  @ColumnDef(
      value = "district",
      type = "varchar(50)"
  )
  public final transient KeyValue<ReceivingAddressDataMap> district = new KeyValue(this, "district", "district", supplier);

  @ColumnDef(
      value = "province",
      type = "varchar(50)"
  )
  public final transient KeyValue<ReceivingAddressDataMap> province = new KeyValue(this, "province", "province", supplier);

  @ColumnDef(
      value = "user_id",
      type = "bigint(21)"
  )
  public final transient KeyValue<ReceivingAddressDataMap> userId = new KeyValue(this, "user_id", "userId", supplier);

  ReceivingAddressDataMap(boolean isTable) {
    super();
    this.isTable = isTable;
  }

  ReceivingAddressDataMap(boolean isTable, int size) {
    super(size);
    this.isTable = isTable;
  }

  /**
   * 创建ReceivingAddressDataMap
   * 初始化主键和gmtCreate, gmtModified, isDeleted等特殊值
   */
  public ReceivingAddressDataMap init() {
    this.id.autoIncrease();
    this.gmtCreate.values(new Date());
    this.gmtModified.values(new Date());
    this.isDeleted.values(false);
    return this;
  }

  public ReceivingAddressDataMap with(Consumer<ReceivingAddressDataMap> init) {
    init.accept(this);
    return this;
  }

  public static ReceivingAddressDataMap table() {
    return new ReceivingAddressDataMap(true, 1);
  }

  public static ReceivingAddressDataMap table(int size) {
    return new ReceivingAddressDataMap(true, size);
  }

  public static ReceivingAddressDataMap entity() {
    return new ReceivingAddressDataMap(false, 1);
  }

  public static ReceivingAddressDataMap entity(int size) {
    return new ReceivingAddressDataMap(false, size);
  }

  /**
   * DataMap数据和表[receiving_address]数据比较
   */
  public ReceivingAddressDataMap eqTable() {
    IDatabase.db.table("receiving_address").query().eqDataMap(this);
    return this;
  }

  /**
   * DataMap数据和表[receiving_address]数据比较
   */
  public ReceivingAddressDataMap eqQuery(String query) {
    IDatabase.db.table("receiving_address").queryWhere(query).eqDataMap(this);
    return this;
  }

  /**
   * DataMap数据和表[receiving_address]数据比较
   */
  public ReceivingAddressDataMap eqQuery(IDataMap query) {
    IDatabase.db.table("receiving_address").queryWhere(query).eqDataMap(this);
    return this;
  }

  /**
   * 清空[receiving_address]表数据
   */
  public ReceivingAddressDataMap clean() {
    IDatabase.db.cleanTable("receiving_address");
    return this;
  }

  /**
   * 插入[receiving_address]表数据
   */
  public ReceivingAddressDataMap insert() {
    IDatabase.db.table("receiving_address").insert(this);
    return this;
  }

  /**
   * 先清空, 再插入[receiving_address]表数据
   */
  public ReceivingAddressDataMap cleanAndInsert() {
    return this.clean().insert();
  }

  public static class Factory {
    public ReceivingAddressDataMap table() {
      return ReceivingAddressDataMap.table();
    }

    public ReceivingAddressDataMap table(int size) {
      return  ReceivingAddressDataMap.table(size);
    }

    public ReceivingAddressDataMap initTable() {
      return ReceivingAddressDataMap.table().init();
    }

    public ReceivingAddressDataMap initTable(int size) {
      return  ReceivingAddressDataMap.table(size).init();
    }

    public ReceivingAddressDataMap entity() {
      return ReceivingAddressDataMap.entity();
    }

    public ReceivingAddressDataMap entity(int size) {
      return  ReceivingAddressDataMap.entity(size);
    }
  }
}
