package cn.org.atool.fluent.mybatis.demo4;

import cn.org.atool.fluent.mybatis.demo4.dm.HelloWorldDataMap;
import cn.org.atool.fluent.mybatis.demo4.dm.ReceivingAddressDataMap;
import cn.org.atool.fluent.mybatis.demo4.dm.UserDataMap;
import cn.org.atool.fluent.mybatis.demo4.mix.HelloWorldTableMix;
import cn.org.atool.fluent.mybatis.demo4.mix.ReceivingAddressTableMix;
import cn.org.atool.fluent.mybatis.demo4.mix.UserTableMix;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import org.test4j.module.database.IDataSourceScript;
import org.test4j.module.spec.annotations.Mix;

/**
 * ATM: Application Table Manager
 *
 * @author Powered By Test4J
 */
public interface ATM {
  /**
   * 应用表名
   */
  interface Table {
    String receivingAddress = "receiving_address";

    String helloWorld = "hello_world";

    String user = "user";
  }

  /**
   * table or entity data构造器
   */
  interface DataMap {
    ReceivingAddressDataMap.Factory receivingAddress = new ReceivingAddressDataMap.Factory();

    HelloWorldDataMap.Factory helloWorld = new HelloWorldDataMap.Factory();

    UserDataMap.Factory user = new UserDataMap.Factory();
  }

  /**
   * 应用表数据操作
   */
  @org.test4j.module.spec.annotations.Mixes
  class Mixes {
    @Mix
    public ReceivingAddressTableMix receivingAddressTableMix;

    @Mix
    public HelloWorldTableMix helloWorldTableMix;

    @Mix
    public UserTableMix userTableMix;

    public void cleanAllTable() {
      this.receivingAddressTableMix.cleanReceivingAddressTable();
      this.helloWorldTableMix.cleanHelloWorldTable();
      this.userTableMix.cleanUserTable();
    }
  }

  /**
   * 应用数据库创建脚本构造
   */
  class Script implements IDataSourceScript {
    @Override
    public List<Class> getTableKlass() {
      return list(
      	ReceivingAddressDataMap.class,
      	HelloWorldDataMap.class,
      	UserDataMap.class
      );
    }

    @Override
    public IndexList getIndexList() {
      return new IndexList();
    }
  }
}
