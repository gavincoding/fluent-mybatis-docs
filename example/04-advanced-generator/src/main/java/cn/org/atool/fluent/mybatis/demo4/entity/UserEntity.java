package cn.org.atool.fluent.mybatis.demo4.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.IEntity;
import java.io.Serializable;
import java.lang.Boolean;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * UserEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "user"
)
public class UserEntity implements IEntity {
  private static final long serialVersionUID = 1L;

  /**
   * 主键id
   */
  @TableId("id")
  private Long id;

  /**
   * 创建时间
   */
  @TableField(
      value = "gmt_create",
      insert = "now()"
  )
  private Date gmtCreate;

  /**
   * 更新时间
   */
  @TableField(
      value = "gmt_modified",
      insert = "now()",
      update = "now()"
  )
  private Date gmtModified;

  /**
   * 是否逻辑删除
   */
  @TableField(
      value = "is_deleted",
      insert = "0"
  )
  private Boolean isDeleted;

  /**
   * 账号
   */
  @TableField("account")
  private String account;

  /**
   * 外键，收货地址id
   */
  @TableField("address_id")
  private Long addressId;

  /**
   * 年龄
   */
  @TableField("age")
  private Long age;

  /**
   * 头像
   */
  @TableField("avatar")
  private String avatar;

  /**
   * 生日
   */
  @TableField("birthday")
  private Date birthday;

  /**
   * 会员积分
   */
  @TableField("bonus_points")
  private Long bonusPoints;

  /**
   * 电子邮件
   */
  @TableField("e_mail")
  private String eMail;

  /**
   * 密码
   */
  @TableField("password")
  private String password;

  /**
   * 电话
   */
  @TableField("phone")
  private String phone;

  /**
   * 状态(字典)
   */
  @TableField("status")
  private String status;

  /**
   * 名字
   */
  @TableField("user_name")
  private String userName;

  @Override
  public Serializable findPk() {
    return this.id;
  }
}
