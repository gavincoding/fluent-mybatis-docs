package cn.org.fluent.mybatis.many2many.demo.test;

import cn.org.fluent.mybatis.many2many.demo.Application;
import cn.org.fluent.mybatis.many2many.demo.entity.MemberEntity;
import cn.org.fluent.mybatis.many2many.demo.entity.MemberLoveEntity;
import cn.org.fluent.mybatis.many2many.demo.mapper.MemberLoveMapper;
import cn.org.fluent.mybatis.many2many.demo.mapper.MemberMapper;
import cn.org.fluent.mybatis.many2many.demo.wrapper.MemberLoveQuery;
import cn.org.fluent.mybatis.many2many.demo.wrapper.MemberQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class FindExFriendsTest {
    @Autowired
    private MemberMapper memberMapper;

    @Test
    public void findExBoyFriends() {
        MemberEntity member = memberMapper.findById(1L);
        System.out.println("是否女孩:" + member.getIsGirl());
        List<MemberEntity> boyFriends = member.findExFriends();
        System.out.println(boyFriends);
    }

    @Autowired
    private MemberLoveMapper loveMapper;

    @Before
    public void setup() {
        memberMapper.delete(new MemberQuery());
        loveMapper.delete(new MemberLoveQuery());
        memberMapper.insertBatch(Arrays.asList(
            new MemberEntity()
                .setId(1L)
                .setIsGirl(true)
                .setUserName("mary")
                .setIsDeleted(false),
            new MemberEntity()
                .setId(2L)
                .setIsGirl(false)
                .setUserName("mike")
                .setIsDeleted(false)
        ));
        loveMapper.insert(new MemberLoveEntity()
            .setGirlId(1L)
            .setBoyId(2L)
            .setStatus("前任")
            .setIsDeleted(false)
        );
    }
}
