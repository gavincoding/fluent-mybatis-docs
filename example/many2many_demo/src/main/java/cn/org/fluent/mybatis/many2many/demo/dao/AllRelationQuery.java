package cn.org.fluent.mybatis.many2many.demo.dao;

import cn.org.atool.fluent.mybatis.functions.IoFunction;
import cn.org.fluent.mybatis.many2many.demo.Refs;
import cn.org.fluent.mybatis.many2many.demo.entity.MemberEntity;
import cn.org.fluent.mybatis.many2many.demo.wrapper.MemberLoveQuery;
import cn.org.fluent.mybatis.many2many.demo.wrapper.MemberQuery;
import org.springframework.stereotype.Service;

import java.util.List;

import static cn.org.fluent.mybatis.many2many.demo.helper.MemberLoveMapping.boyId;
import static cn.org.fluent.mybatis.many2many.demo.helper.MemberLoveMapping.girlId;

@Service
public class AllRelationQuery extends Refs {
    @Override
    public List<MemberEntity> findExFriendsOfMemberEntity(MemberEntity entity) {
        return findExFriendsOfMemberEntity3(entity);
    }

    public List<MemberEntity> findExFriendsOfMemberEntity3(MemberEntity entity) {
        MemberQuery query = new MemberQuery()
            .where.isDeleted().isFalse()
            .and.id().in(MemberLoveQuery.class, q -> q
                .select(entity.getIsGirl() ? boyId.column : girlId.column)
                .where.status().eq("前任")
                .and.isDeleted().isFalse()
                .and.girlId().eq(entity.getId(), o -> entity.getIsGirl())
                .and.boyId().eq(entity.getId(), o -> !entity.getIsGirl())
                .end())
            .end();
        return memberMapper.listEntity(query);
    }

    public List<MemberEntity> findExFriendsOfMemberEntity2(MemberEntity entity) {
        if (entity.getIsGirl()) {
            IoFunction<MemberLoveQuery> nest = q -> q
                .select.boyId().end()
                .where.status().eq("前任")
                .and.isDeleted().isFalse()
                .and.girlId().eq(entity.getId())
                .end();
            MemberQuery query = new MemberQuery()
                .where.isDeleted().isFalse()
                .and.id().in(MemberLoveQuery.class, nest)
                .end();
            return memberMapper.listEntity(query);
        } else {
            IoFunction<MemberLoveQuery> nest = q -> q
                .select.girlId().end()
                .where.status().eq("前任")
                .and.isDeleted().isFalse()
                .and.boyId().eq(entity.getId())
                .end();
            MemberQuery query = new MemberQuery()
                .where.isDeleted().isFalse()
                .and.id().in(MemberLoveQuery.class, nest)
                .end();
            return memberMapper.listEntity(query);
        }
    }

    public List<MemberEntity> findExFriendsOfMemberEntity1(MemberEntity entity) {
        IoFunction<MemberLoveQuery> nest1 = q -> q
            .select.boyId().end()
            .where.status().eq("前任")
            .and.isDeleted().isFalse()
            .and.girlId().eq(entity.getId())
            .end();
        IoFunction<MemberLoveQuery> nest2 = q -> q
            .select.girlId().end()
            .where.status().eq("前任")
            .and.isDeleted().isFalse()
            .and.boyId().eq(entity.getId())
            .end();
        MemberQuery query = new MemberQuery()
            .where.isDeleted().isFalse()
            .and.id().in(entity.getIsGirl(), MemberLoveQuery.class, nest1)
            .and.id().in(!entity.getIsGirl(), MemberLoveQuery.class, nest2)
            .end();
        return memberMapper.listEntity(query);
    }

    @Override
    public MemberEntity findCurrFriendOfMemberEntity(MemberEntity entity) {
        MemberQuery query = new MemberQuery()
            .where.isDeleted().isFalse()
            .and.id().in(MemberLoveQuery.class, q -> q
                .select(entity.getIsGirl() ? boyId.column : girlId.column)
                .where.status().eq("现任")
                .and.isDeleted().isFalse()
                .and.girlId().eq(entity.getId(), o -> entity.getIsGirl())
                .and.boyId().eq(entity.getId(), o -> !entity.getIsGirl())
                .end())
            .end();
        return memberMapper.findOne(query);
    }
}
