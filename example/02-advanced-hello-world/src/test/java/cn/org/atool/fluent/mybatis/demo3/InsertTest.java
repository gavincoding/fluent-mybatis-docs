package cn.org.atool.fluent.mybatis.demo3;

import cn.org.atool.fluent.mybatis.HelloWorldConfig;
import cn.org.atool.fluent.mybatis.demo3.mapper.HelloWorld3Mapper;
import cn.org.atool.fluent.mybatis.demo3.entity.HelloWorld3Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HelloWorldConfig.class)
public class InsertTest {
    @Autowired
    HelloWorld3Mapper mapper;

    @Test
    public void testInsertDefaultValue() {
        // 全表清空
        mapper.delete(mapper.query().where.apply("1=1").end());
        // 插入记录，未设置gmtCreated, gmtModified, isDeleted3个字段
        mapper.insert(new HelloWorld3Entity()
            .setSayHello("hello")
            .setYourName("fluent mybatis")
        );
        // 查询并在控制台输出记录
        HelloWorld3Entity entity = mapper.findOne(mapper.query()
            .where.sayHello().eq("hello")
            .and.yourName().eq("fluent mybatis").end()
        );
        System.out.println(entity);
    }
}