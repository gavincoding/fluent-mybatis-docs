# FluentMybatis: 基于mybatis但青出于蓝
- **No XML, No Mapper, No If else, No String魔法值编码**
- **只需Entity就实现强大的FluentAPI: 支持分页, 嵌套查询, AND OR组合, 聚合函数...**

## [Fluent-Mybatis特性](docs/fluent-mybatis-feature.md)
![-w930](docs/images/fluent-mybatis-feature.png)

## [Fluent-Mybatis基本原理](docs/01-hello-world/README.md)
![-w930](docs/images/fluent-mybatis-driver.png)

## Fluent Mybatis文档
- [Hello World - 1, 快速入门](docs/01-hello-world/README.md)
- [Hello World - 2, 快速入门](docs/02-advanced-hello-world/README.md)
- [Fluent MyBatis使用入门一](docs/spring-boot-demo/README.md)
- [Fluent MyBatis使用入门二](docs/spring-boot-demo/Fluent-Mybatis-StepByStep2.md)
- [Fluent MyBatis使用入门三:复杂查询, 关联查询](docs/spring-boot-demo/StepByStep-Join.md)
- [Fluent MyBatis使用入门四:处理多对多关系](docs/many2many_demo/many_to_many.md)
- [Fluent Mybatis生成Entity文件](docs/03-entity-generator/README.md)

## Fluent Mybatis语法
- [显式指定select字段](docs/05-more-syntax/segment/01-select.md)
- [select聚合函数](docs/05-more-syntax/segment/02-select-aggregate.md)
- [查询(更新)条件](docs/05-more-syntax/segment/03-where.md)
- [嵌套查询](docs/05-more-syntax/segment/04-nested-where.md)
- [GroupBy设置](docs/05-more-syntax/segment/05-groupBy.md)
- [排序设置](docs/05-more-syntax/segment/06-orderBy.md)
- [limit设置](docs/05-more-syntax/segment/07-limit.md)
- [Update设置](docs/05-more-syntax/segment/08-update.md)

## 和其它Mybatis增强框架比较
- [Fluent Mybatis, 原生Mybatis, Mybatis Plus三者功能对比](docs/06-other/compare-mybatis.md)
- [剖析Mybatis Plus实现动态SQL语句的机理](docs/06-other/compare-mp-and-fm-01.md)
- [Fluent Mybatis动态SQL构造的机理](docs/06-other/compare-mp-and-fm-02.md)


## Fluent-Mybatis源码
[Fluent Mybatis Gitee](https://gitee.com/fluent-mybatis/fluent-mybatis)

[Fluent Mybatis GitHub](https://github.com/atool/fluent-mybatis) 

## [Fluent-Mybatis 历程](docs/fluent-mybatis-history.md)

