
## 使用fluent mybatis生成Entity文件
增加生成代码的jar包依赖
```xml
<dependency>
    <groupId>com.github.atool</groupId>
    <artifactId>fluent-mybatis-processor</artifactId>
    <version>${fluent.mybatis.version}</version>
    <scope>provided</scope>
</dependency>
```

### 使用注解定义方式
```java
public class EntityGeneratorDemo {
    public static final String url = "jdbc:mysql://localhost:3306/fluent_mybatis?useUnicode=true&characterEncoding=utf8";

    public static void main(String[] args) throws Exception {
        FileGenerator.build(Empty.class);
    }

    @Tables(
        // 设置数据库连接信息
        url = url, username = "root", password = "password",
        // 设置entity类生成src目录, 相对于 user.dir
        srcDir = "03-entity-generator/src/main/java",
        // 设置entity类的package值
        basePack = "cn.org.atool.fluent.mybatis.demo3",
        // 设置dao接口和实现的src目录, 相对于 user.dir
        daoDir = "03-entity-generator/src/main/java",
        // 设置哪些表要生成Entity文件
        tables = {@Table(value = {"hello_world"})}
    )
    static class Empty {
    }
}
```

### 使用java编码设置方式
使用EntityGenerator 生成简单的Entity映射代码，只需要做
1. globalConfig: 代码生成器的全局设置，主要包括
    1. 设置文件路径和package路径
    2. 设置数据库链接信息（url, username, password)
2. tables: 需要生成实体类的表
3. execute: 根据步骤1和2的设置, 生成文件

```java
public class EntityGeneratorDemo {
    public static void main(String[] args) throws Exception {
        FileGenerator.build(true, false)
            .globalConfig(g -> g
                /** 设置src/java 所在的路径 **/
                .setOutputDir(baseDir + "src/main/java/")
                /** 设置package **/
                .setBasePackage("cn.org.atool.fluent.mybatis.demo3")
                /** 设置数据库连接 **/
                .setDataSource(url, "root", "password")
            )
            .tables(t -> t
                /** 设置需要生成Entity类的表 **/
                .table("hello_world")
                //.table("其它表")
                //.table("其它表")
            )
            .execute();
    }
}
```